import * as express from 'express'
import { createConnection, getConnection } from 'typeorm'
import { initialize } from '../src'

async function startServer() {
    try {
        // Close any existing database connection
        try {
            await getConnection().close()
        } catch (error) { }

        // Establish database connection
        await createConnection()

        // Start up our express server
        const app = express()
        const port = 3000

        // Parse request body
        app.use(express.json())

        // Prettify the json in the browser for easier reading
        app.set('json spaces', 4)
        
        // Add your route methods
        initialize(app)

        // Communicate that the server is running
        app.listen(port, () => console.log(`Application served on port ${port}!`))
    } catch (error) {
        // Something failed, exit the process
        console.log('error while initializing', error)
        process.exit(1)
    }
}

startServer()