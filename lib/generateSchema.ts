import * as models from '../models'
import { createConnection } from 'typeorm'
import { performance } from 'perf_hooks'
import { readFileSync } from 'fs'

// Start the timer for the script
const startTime = performance.now()

// Establish a connection to the database
createConnection().then(async connection => {
    // Reset database schema
    for (const query of readFileSync('db/schema.sql').toString().split(';')) {
        if (query.trim() !== '') {
            try {
                await connection.query(query)
            } catch (error) {
                console.error(error)
                process.exit(1)
            }
        }
    }

    // Create respositories
    const userRepository = connection.manager.getRepository(models.User)
    const postRepository = connection.manager.getRepository(models.Post)

    // Create users - option 1
    await userRepository.save(userRepository.create({
        username: 'Ben',
        password: 'pearagri'
    }))

    // Create users - option 2
    const users = [
        { username: 'Luke', password: 'mercervalve' },
        { username: 'Matt', password: 'businessman' },
        { username: 'Sam', password: 'businesssam' }
    ]

    const userModels = users.map(user => {
        const userModel = new models.User()
        userModel.username = user.username
        userModel.password = user.password

        return userModel
    })

    // Save users to the database
    await userRepository.save(userModels)

    // Get the users
    const ben = await userRepository.findOne({ where: { id: 1 }})
    const luke = await userRepository.findOne({ where: { id: 2 }})
    const matt = await userRepository.findOne({ where: { id: 3 }})
    const sam = await userRepository.findOne({ where: { id: 4 }})

    // Posts we want to add to the database
    const posts = [
        { user: ben, body: 'The memory we used to share is no longer coherent.' },
        { user: ben, body: 'She borrowed the book from him many years ago and has not yet returned it.' },
        { user: ben, body: 'A purple pig and a green donkey flew a kite in the middle of the night and ended up sunburnt.' },
        { user: luke, body: 'Is it free?' },
        { user: luke, body: 'If the Easter Bunny and the Tooth Fairy had babies would they take your teeth and leave chocolate for you?' },
        { user: luke, body: 'I was very proud of my nickname throughout high school but today- I couldn’t be any different to what my nickname was.' },
        { user: matt, body: 'I want more detailed information.' },
        { user: matt, body: 'The waves were crashing on the shore; it was a lovely sight.' },
        { user: matt, body: 'If Purple People Eaters are real… where do they find purple people to eat?' },
        { user: sam, body: 'The quick brown fox jumps over the lazy dog.' },
        { user: sam, body: 'I checked to make sure that he was still alive.' },
        { user: sam, body: 'A song can make or ruin a person’s day if they let it get to them.' }
    ]

    // Map over the posts array to create Post models
    const postModels = posts.map(post => {
        const postModel = new models.Post()
        postModel.creator = post.user
        postModel.body = post.body
        
        return postModel
    })

    // Save posts to the database
    await postRepository.save(postModels)

    // We're done, close the connection to the database
    await connection.close()

    // Let the user know the schema has been generated
    const finishTime = performance.now()
    console.log(`The schema was successfully generated in ${((finishTime - startTime) / 1000).toFixed(2)} seconds.`)
})
