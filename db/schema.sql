DROP DATABASE IF EXISTS  twitter;
CREATE DATABASE twitter CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';
USE twitter;



CREATE TABLE application_user (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  username varchar(20) NOT NULL,
  password_text varchar(16) NOT NULL,
  age tinyint(4) DEFAULT NULL,
  bio varchar(5000) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE post (
  PRIMARY KEY (id),
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  creator_id BIGINT unsigned NOT NULL,
  body varchar(140) NOT NULL,
  dateCreated date DEFAULT curdate() COMMENT 'added by jim',
  likeFLAG tinyint(4) DEFAULT NULL,
  KEY creator_id (creator_id),
  CONSTRAINT FOREIGN KEY (creator_id) REFERENCES application_user (id)
);

