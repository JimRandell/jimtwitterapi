import { Express, Request, Response } from 'express'
import { User, Post } from '../models'
import { getConnection, getRepository } from 'typeorm'

export function initialize(app: Express) {
	app.delete('/posts', async (request: Request, response: Response) => {
		if (request.query.hasOwnProperty('delete')) {
			const delete1 = await getRepository(Post).find()
			await getConnection()
				.createQueryBuilder()
				.delete()
				.from(Post)
				.where('id = :id', { id: request.query.id })
				.execute()
			response.send(`success:${request.query.id}`)
		}
	})

	app.put('/posts', async (request: Request, response: Response) => {
		const post = await getRepository(Post)
			.createQueryBuilder('post')
			.where('id = :id', { id: request.query.id })
			.getOne()

		if (post) {
			const result = request.query.hasOwnProperty('updateBody')
				? await getConnection()
					.createQueryBuilder()
					.update(Post)
					.set({ body: request.query.updateBody })
					.where('id = :id', { id: request.query.id })
					.execute()
				: request.query.hasOwnProperty('like')
				? await getConnection()
					.createQueryBuilder()
					.update(Post)
					.set({ likeFLAG: post.likeFLAG === 1 ? 0 : 1 })
					.where('id = :id', { id: request.query.id })
					.execute()
				: ''
			return response.send(result)
		} else return response.send('post not found')
	})

	app.get('/posts', async (request: Request, response: Response) => {
		const posts = await getRepository(Post).find()
		let result: any
		result
			= request.query.hasOwnProperty('all')
			? posts

			: request.query.hasOwnProperty('bodyLength')
			? posts.filter(item => item.body!.length > request.query.bodyLength)

			: request.query.hasOwnProperty('userid')
			? posts.filter(item => item.userId === request.query.userid)

			: request.query.hasOwnProperty('formatted')
			? posts.map(data => {return `<h4 style=color:red>PostID:${data.id}</h4><h1>${data.body}</h1></br>`}).join('')

			: ''
		response.send(result)
	})

	app.get('/users', async (request: Request, response: Response) => {
		const users = await getRepository(User).find()
		response.send(users)
	})

	app.get('/users/id', async (request: Request, response: Response) => {
		const user = await getRepository(User)
			.createQueryBuilder('user')
			.where('user.id = :id', { id: request.query.id })
			.getOne()
		response.send(user)
	})

	app.post('/users/', async (request: Request, response: Response) => {
		const newPost = await getConnection()
			.createQueryBuilder()
			.insert()
			.into(Post)
			.values([{ body: request.query.body, creator: request.query.id, userId: request.query.id }])
			.execute()
		response.send(newPost)
	})
}
