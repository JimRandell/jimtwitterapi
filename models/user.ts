import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { Post } from './post'

/**
 * An entity who is authorized to use the application
 */
@Entity({ name: 'application_user' })
export class User {
	/**
	 * Age
	 */
	@Column({ name: 'age', type: 'tinyint' })
	age?: number

	/**
	 * a bit of info about the user
	 */
	@Column({ name: 'bio', type: 'varchar' })
	bio?: number

	/**
	 * Identifier
	 */
	@PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
	id?: number

	/**
	 * User who created the post
	 */
	@OneToMany(type => Post, post => post.creator)
	posts?: Post[]

	/**
	 * Username
	 */
	@Column({ name: 'username', type: 'varchar' })
	username?: string

	/**
	 * Password
	 */
	@Column({ name: 'password_text', type: 'varchar' })
	password?: string
}
