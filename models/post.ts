import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm'
import { User } from './user'

/**
 * A post (tweet)
 */
@Entity({ name: 'post' })
export class Post {


	/**
	 * Date the Post was Created
	 */
	@Column({ name: 'dateCreated', type: 'date' })
	dateCreated?: Date

	/**
	 * post was liked
	 */
	@Column({ name: 'likeFLAG', type: 'tinyint' })
	likeFLAG?: Number

	/**
	 * Identifier
	 */
	@PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
	id?: Number


	/**
	 * User who created the post
	 */
	@ManyToOne(type => User, user => user.posts)
	@JoinColumn({ name: 'creator_id' })
	creator?: User

	/**
	 * Post body
	 */
	@Column({ name: 'body', type: 'varchar' })
	body?: String


		/**
	 * Identifier
	 */
	@Column({ name: 'creator_id', type: 'bigint' })
	userId?: Number

}

