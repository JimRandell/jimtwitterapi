# Twitter API

Following on from the Twitter clone challenge and a brief intro to SQL you will now create your very own API using Express.js, MariaDB and TypeORM.

## Before you get started

You will need the following:

* MariaDB Server 10.4 Stable
* Postman (API Development Environment)

Make sure you have followed through up to *Altering Tables in MariaDB* in the [Beginner MariaDB Articles](https://mariadb.com/kb/en/library/a-mariadb-primer/) section of their Knowledge Base.

## The Goal

The primary goal is to learn how to use TypeORM and SQL and prepare you for the world of Milkman.

## Getting Started

* Clone/Fork this repository
* Install node modules `npm install`
* Create a blank database
* Update `ormconfig.json` and `db/schema.sql` to suit your local settings
* To run with hot-reloading (recommended): `npm run start:watch`
* To run without hot-reloading: `npm start`

## Tasks

Configure your app in `src/index.ts` to do the following:

* Retrieve all posts
* Find and display posts longer than 50 characters
* Display posts for a specific user
* Display posts in a 'pretty' format (html)
* Add date created to posts (update schema, model, generateSchema)
* Complete the POST route method on `line 22 src/index.ts`
* Create a PUT route method to enable editing of a specific post
* Create a DELETE route method to delete a specific post
* Add an age and bio for users
* Add the ability to like/unlike a post
